# Stamp-back

**Stamp-back** is the messenger backend repository. The main task of implementing this messenger is learning new things, using new and interesting technologies in a real project.

The repository for the frontend part of this project is located [here](https://gitlab.com/stamp1/stamp-web).

## Installation

Use the [Docker](https://pip.pypa.io/en/stable/) to install this in repository path.

```bash
docker-compose up -d --build
```

## Usage

To check the server operation, you can go to the address:

```shell script
localhost:3000
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.