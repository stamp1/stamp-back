FROM node:12.16

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY package.json package.json

RUN npm install

COPY . .

EXPOSE 443

USER node

CMD ["npm", "run", "watch"]
