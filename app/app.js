require('dotenv').config();
const mongoose      = require('mongoose');
const fs = require("fs");
const path = require("path");
const bodyParser    = require('body-parser').json();
const logger        = require('morgan')('dev');
const SocketSingleton= require('./services/socket-io-singleton')
const cors          = require('cors');
const app           = require('express')();
const config        = require('./configs/config');
const server        = require('https').createServer({
    key: fs.readFileSync(config.certs.key),
    cert: fs.readFileSync(config.certs.cert)
}, app);

const auth          = require('./middlewares/auth.middleware');
const authRouter    = require('./routes/auth.route');
const userRouter    = require('./routes/user.route');
const chatRouter  = require('./routes/chat.route');
const fileRouter    = require('./routes/file.route');
const paymentRouter = require('./routes/payment.route');

// init mongoose
mongoose.connect(config.db.mongodbURI, config.db.options)
    .then( () =>  console.log('MongoDB connect') )
    .catch( (e) => console.error(e) );

// init express middlewares
app.use(bodyParser);
app.use(logger);
app.use(cors({
    origin: process.env.APP_URL
}));
// app.use('/files', express.static(__dirname + '/.data/files'));

// init express routes
app.use('/auth', authRouter);
app.use('/user', auth.authenticate, userRouter);
app.use('/chat', auth.authenticate, chatRouter);
app.use('/file', fileRouter);
app.use('/file', paymentRouter);

SocketSingleton.configure(server);

// const io = require('socket.io')(server);
//
// io.on('connection', function (socket) {
//     socket.emit('news', { hello: 'world' });
//     socket.on('test', function (data) {
//         console.log('test');
//     });
// });

app.use((err, req, res, next) => {
    res.status(500).json({error: err.stack});
    next();
})

module.exports = {
    server
};
