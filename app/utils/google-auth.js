const {google} = require('googleapis')
const people = google.people('v1');

/*******************/
/** CONFIGURATION **/
/*******************/

const googleConfig = {
    clientId: process.env.GOOGLE_CLIENT_ID, // e.g. asdfghjkljhgfdsghjk.apps.googleusercontent.com
    clientSecret: process.env.GOOGLE_CLIENT_SECRET, // e.g. _ASDFA%DFASDFASDFASD#FAD-
    redirect: process.env.GOOGLE_REDIRECT_URL, // this must match your google api settings
};

const defaultScope = [
    'profile',
    'https://www.googleapis.com/auth/userinfo.email'
];

const oauth2Client = new google.auth.OAuth2(
    googleConfig.clientId,
    googleConfig.clientSecret,
    googleConfig.redirect
);

google.options({auth: oauth2Client});

/*************/
/** HELPERS **/
/*************/

/**********/
/** MAIN **/
/**********/

/**
 * Part 1: Create a Google URL and send to the client to log in the user.
 */
function urlGoogle() {
    return oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: defaultScope.join(' '),
    });
}

/**
 * Part 2: Take the "code" parameter which Google gives us once when the user logs in, then get the user's email and id.
 */
async function getGoogleAccountFromCode(code) {
    const {tokens} = await oauth2Client.getToken(code);
    oauth2Client.credentials = tokens;
    const res = await people.people.get({
        resourceName: 'people/me',
        personFields: 'emailAddresses',
    });
    return res.data.emailAddresses[0]
    // const userGoogleId = me.data.id;
    // const userGoogleEmail = me.data.emails && me.data.emails.length && me.data.emails[0].value;
    // return {
    //     id: userGoogleId,
    //     email: userGoogleEmail,
    //     tokens: tokens,
    // };
}

module.exports = {
    urlGoogle,
    getGoogleAccountFromCode,
}
