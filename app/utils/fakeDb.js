// this script can put in db test data about users
const fetch = require('node-fetch');
const config = require('../configs/config');
const api = `http://${config.server.host}:${config.server.port}`;

fetch(api+'/auth/register/', {
    method: 'post',
    body: JSON.stringify({
        login: 'admin',
        password: 'password'
    }),
    headers: { 'Content-Type': 'application/json' },
    })
    .then(res => res.text())
    .then(body => console.log(body));