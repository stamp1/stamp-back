const app       = require('./app').server;
const config      = require('./configs/config');

app.listen(config.server.port, config.server.host);