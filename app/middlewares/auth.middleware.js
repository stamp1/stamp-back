const jwt = require('jsonwebtoken');
const { passport } = require('../configs/config');
// const redis = require('../services/redis-clients');
const lt = require('long-timeout');
const Sockets = require('../services/socket-io-singleton');

let clients = {};

function decodeToken(token) {
    try {
        return jwt.verify(token, passport.privateKey);
    } catch (e) {
        return false;
    }
}

function authenticate(req, res, next) {
    if (typeof req.header('Authorization') === 'undefined') {
        res.status(500).json({
            message: 'Missed Authorization header'
        })
    }
    let token = req.header('Authorization').replace('Bearer ', '');
    let tokenPayload = decodeToken(token);
    if (token && tokenPayload) {
        req.user = tokenPayload.user;
        req.token = token;
        next();
    } else {
        res.status(401).json({
           message: 'Auth error'
        });
    }
}

function authenticateWebSocket(socket, next) {
    const token = socket.handshake.query.token;
    const decodedToken = decodeToken(token);
    if (token && decodedToken) {
        const user = decodedToken['user'];
        const userId = user._id;
        const socketId = socket.id;

        typeof clients[userId] === 'undefined' ? clients[userId] = [socketId] : clients[userId].push(socketId);
        socket.user = user;
        socket.expireTimer = lt.setTimeout(function () {
            socket.disconnect(true);
        }, decodedToken.exp * 1000 - Date.now() - 20000);
        return next();
    }
    return next(new Error('authentication error'));
}

module.exports = {
    authenticate,
    authenticateWebSocket,
    clients
};
