const mongoose      = require('mongoose');
const { Schema }    = mongoose;

const FileSchema = new Schema({
    path: {
        type: String,
        required: true,
    },

    type: {
        type: String,
        required: true,
    },

    owner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true,
    },

    destination: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'message',
        required: false,
    }
},{
    timestamps: true
});
module.exports = mongoose.model('file', FileSchema);
module.exports.schema = FileSchema;