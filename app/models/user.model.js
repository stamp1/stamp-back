const mongoose      = require('mongoose');
const { Schema } = mongoose;
const chat = require('./chat.model.js');
const Group = require('./group.model.js');

const UserSchema = new Schema({
    login: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true,
        select: false,
    },
    username: {
        type: String,
        maxlength: 40,
    },
    options: {
        description: {
            type: String,
            maxlength: 200,
        },
        image: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'file',
        },
        google: {
            email: String,
        }
    },
    chats: {
        type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'chat'}],
        ref: 'chat',
        select: false,
    }
},{
    timestamps: true
});
module.exports = mongoose.model('user', UserSchema);
