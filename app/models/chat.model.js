const mongoose      = require('mongoose');
const { Schema }    = mongoose;

const ChatSchema = new Schema({
    chatType: {
        type: String,
        required: true,
        default: 'dialog'
    },
    interlocutors: [{
        type : mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true,
    }],
    messages: [{
        type : mongoose.Schema.Types.ObjectId,
        ref: 'message',
    }]
},{
    timestamps: true
});
module.exports = mongoose.model('chat', ChatSchema);