const mongoose      = require('mongoose');
const { Schema }    = mongoose;
const User          = require('./user.model');

const MessageSchema = new Schema({
    mType: {
        type: String,
        required: true,
    },

    content: {
        type: String,
        required: true,
    },

    sender: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true,
    },

    status: {
        type: String,
        default: 'unread',
        required: true,
    },

    replied: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'message',
        required: false,
    }
},{
    timestamps: true
});
module.exports = mongoose.model('message', MessageSchema);
module.exports.schema = MessageSchema;