const config = require('../configs/config')

// init Redis client
const redis = require("redis");
const client = redis.createClient({
    host: config.redis.host
});
client.on( "error", err => console.log("Redis Error " + err) );
client.on( "ready", _ => console.log("Redis Ready ") );


// TODO: Make redis store for clients
module.exports = client;
