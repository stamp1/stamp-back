const User = require('../models/user.model');

module.exports.getUserByLogin = async (login, needPass = false) => {
    return ( await User.findOne({login}).select(needPass ? '+password' : '') )
}

module.exports.getUserByGoogleEmail = async email => {
    return ( await User.findOne({ "options.google.email" : email }).select('+password') )
}
