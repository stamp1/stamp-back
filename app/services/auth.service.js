const UserModel = require('../models/user.model');
const UserService = require("./user.service");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const {passport} = require("../configs/config");

async function generateToken(candidate) {
    let user = {
        user: candidate
    }
    return jwt.sign(
        user,
        passport.privateKey,
        passport.options
    )
}

module.exports.generateToken = generateToken

module.exports.login = async (login, password) => {
    let result;

    const candidate = await UserService.getUserByLogin(login, true);

    if (candidate) {
        const passwordCheck = bcrypt.compareSync(password, candidate.password);
        if (passwordCheck) {
            candidate.password = '';
            candidate.password = '';

            const token = await generateToken(candidate)

            result = {
                code: 200,
                message: 'Successfully logged in!',
                token: `Bearer ${token}`
            }
        } else {
            result = {
                code: 401,
                message: 'Invalid password entered.'
            }
        }
    } else {
        result = {
            code: 404,
            message: 'This UserModel does not exist.'
        }
    }
    return result;
}

module.exports.register = async (login, password, googleEmail = false) => {
    let result;

    const candidate = await UserService.getUserByLogin(login, true);

    if (candidate) {
        result = {
            code: 409,
            message: 'User already exist'
        }
    } else {
        const salt = bcrypt.genSaltSync(10);
        const UserModelPassword = bcrypt.hashSync(password, salt);
        const user = new UserModel({
            login,
            password: UserModelPassword
        });

        try {
            if (googleEmail) {
                user.options.google.email = googleEmail;
            }

            await user.save();
            result = {
                code: 201,
                message: 'Successful registration'
            }
        } catch (e) {
            result = {
                code: 409,
                message: 'Unknown registration error'
            }
            console.error(e);
        }
    }
    return result;
}

module.exports.logout = async () => {
    return {
        message: 'Succesfuly logouted',
        code: 205
    }
}
