const auth                  = require("../middlewares/auth.middleware");
const {clients} = require("../middlewares/auth.middleware");
const lt = require('long-timeout');

class SocketIOServer {
    configure(server) {
        const io = require('socket.io')(server);

        this.socket = io;
        io.use(auth.authenticateWebSocket);
        io.on('connection', this.setupEvents);
        io.on('connection', this.setupConnectionManager);
    }

    setupEvents(connection) {
        connection.on('disconnect', _ => {
            if (typeof clients[connection.user._id] !== 'undefined') {
                clients[connection.user._id] = clients[connection.user._id].filter(socketid => connection.id !== socketid);
                lt.clearTimeout(connection.expireTimer);
            };
        });
    }

    sendToUser(userId, type, data) {
        if (typeof clients[userId] !== 'undefined')
            clients[userId].forEach( client => this.socket.to(client).emit(type, data) );
    }

    sendToSocket(socketId, type, data) {
        this.socket.to(socketId).emit(type, data);
    }

    // TODO: Make online/offline statuser
    setupConnectionManager(connection) {
        connection.on('set_online', data => {
            console.log(`${connection.user.login} online: ${true}`);
        });
        connection.on('set_offline', _ => {

        })
        connection.on('disconnect', _ => {
            console.log(`${connection.user.login} online: ${false}`);
        })
    }
}

module.exports = (function () { return new SocketIOServer() })();