const formidable    = require('formidable');
const fsP           = require('fs').promises;
const existsSync    = require('fs').existsSync;
const path          = require('path');
const File          = require('../models/file.model')
const { compress }  = require('compress-images/promise');


const getCleanFilePath = (filePath) => {
    if (existsSync(filePath)) {
        const filePathArray = filePath.split('.');
        const fileExtension = filePathArray.pop();
        filePath = filePathArray.join('.');
        if (!fileExtension) new Error('Bad file extention');
        filePath = filePath + '(new)' + fileExtension;
    }
    return filePath;
}


module.exports = {
    getFormData: (req, opts) => {
        return new Promise( (resolve, reject) => {
            const form = formidable(opts);
            form.parse(req, (err, fields, files) => {
                if (err) reject(err);
                resolve({ fields, files });
            });
        })
    },
    saveFile: async (file, owner, destination ) => {
        let tempPath = file.path;
        let filePath = '';
        try {
            await fsP.copyFile(tempPath, path.join(__dirname, '../../../uploads/uncompressed/')+file.name)
            await fsP.unlink(tempPath)
            tempPath = path.join(__dirname, '../../../uploads/uncompressed/')+file.name;
            // filePath = getCleanFilePath(path.join(__dirname, '../../uploads/'));
            filePath = path.join(__dirname, '../../../uploads/');
            const { statistics } = await compress({
                source: tempPath,
                destination: filePath,
                enginesSetup: {
                    jpg: { engine: 'mozjpeg', command: ["-quality", "60"]},
                    png: { engine: 'pngquant', command: ["--quality=20-50"]},
                }
            })
            filePath = statistics[0]['path_out_new'];
            await fsP.unlink(tempPath);
        } catch (e) {
            console.log(e);
        }
        const fileInstance = new File({
            type: 'image',
            path: filePath,
            owner: owner._id,
            destination: destination
        });

        try {
            fileInstance.save();
        } catch (e) {
            console.log(e);
        }
        return fileInstance;
    },
    getFile: (req, fileId) => {

    },
}