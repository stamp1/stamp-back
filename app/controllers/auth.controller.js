const AuthService = require("../services/auth.service");
const UserService = require("../services/user.service");
const {getGoogleAccountFromCode} = require("../utils/google-auth");
const {urlGoogle} = require("../utils/google-auth");

module.exports.login = async (req, res) => {
    let {login, password} = req.body;

    const result = await AuthService.login(login, password);

    res.status(result.code).json(result);
};

module.exports.register = async (req, res) => {
    let {login, password} = req.body;

    const result = await AuthService.register(login, password);

    res.status(result.code ? result.code : 500).json(result);
};

// todo: make logout and token deactivation system
module.exports.logout = async (req, res) => {
    const result = await AuthService.logout();

    res.status(result.code).json(result);
};

module.exports.getGoogleAuth = (req, res) => {
    res.redirect(urlGoogle());
};

module.exports.googleRedirectCallback = async (req, res) => {
    const resp = await getGoogleAccountFromCode(req.query.code);
    if (resp && resp.value) {
        let candidate = await UserService.getUserByGoogleEmail(resp.value)
        let result;
        if (candidate) {
            result = {
                token: await AuthService.generateToken(candidate)
            }
        } else {
            let registration = await AuthService.register(
                    resp.value,
                Math.random().toString(36).substring(7), resp.value
            );
            candidate = await UserService.getUserByGoogleEmail(resp.value)
            if (registration.code === 201) {
                result = {
                    token: await AuthService.generateToken(candidate)
                }
            }
        }
        res.redirect(process.env.APP_URL+'/#/?token='+result.token)
    }
};
