const Chat = require('../models/chat.model');
const Message = require('../models/message.model');
const User = require('../models/user.model');
const Sockets = require('../services/socket-io-singleton')
const SocketTypes = require("../entities/socket-types");

async function createChat(recipientId, senderId) {

    const interlocutorsIDs = [recipientId, senderId];

    let candidate = await Chat.findOne({
        "interlocutors": {"$all": interlocutorsIDs}
    });

    if (candidate) {
        return false;
    }

    const recipient = await User.findOne({ _id: recipientId }).select('+chats');
    const sender = await User.findOne({ _id: senderId }).select('+chats');

    const chat = new Chat({
        interlocutors: [recipient, sender]
    });

    recipient.chats.push(chat._id);
    sender.chats.push(chat._id);

    try {
        await chat.save();
        await recipient.save();
        await sender.save();
    } catch (e) {
        console.log(e);
    }

    try {
        return await Chat.findOne(chat._id);
    } catch (e) {
        console.error(e);
    }
}


module.exports.sendMessage = async (req, res) => {
    const chatId = req.body.chatId;
    const senderId = req.user._id;
    const messageType = req.body.type;
    const messageContent = req.body.content;
    const replied = req.body.replied;

    // todo: user express validator
    // validate fields
    if (!chatId) {
        await res.status(500).json({
            'message': 'Set chatId field'
        });
        return;
    }
    if (!senderId) {
        await res.status(500).json({
            'message': 'SenderId field is undefined'
        });
        return;
    }
    if (!messageType) {
        await res.status(500).json({
            'message': 'Set messageType field'
        });
        return;
    }
    if (!messageContent) {
        await res.status(500).json({
            'message': 'Set messageContent field'
        });
        return;
    }

    const chatCandidate = await Chat.findOne({
        "_id": chatId
    });

    if (!chatCandidate) {
        await res.status(404).json({
            'message': 'Chat not found'
        });
        return;
    };

    const sender = await User.findOne({
        "_id": senderId
    });

    if (!sender) {
        await res.status(404).json({
            'message': 'Sender not found'
        });
        return;
    }

    const message = await new Message({
        mType: messageType,
        content: messageContent,
        sender: sender,
    });

    if (replied) {
        message.replied = replied;
    }

    try {
        await message.save();
    } catch (e) {
        console.error(e);
        // await res.status(500).json({
        //     'message': 'Error in message saving'
        // });
        res.status(500).json({
            'message': e.message
        });
        return;
    }

    chatCandidate.messages.push(message);

    try {
        await chatCandidate.save();
    } catch (e) {
        console.error(e);
        await res.status(500).json({
            'message': 'Error in chat saving'
        });
        return;
    }

    const messageToResponse = message.depopulate("sender");
    await res.status(200).json({
        message: messageToResponse,
        destination: chatId
    });
    const recipient = chatCandidate.interlocutors.find( id => id.toString() !== senderId.toString());
    Sockets.sendToUser( recipient, 'new_chat_message', {
        message: messageToResponse,
        destination: chatId
    });
    return;
};

module.exports.changeMessage = async (req, res) => {
    const messageId = req.params.messageId;
    const content = req.body.content;
    const currentUserId = req.user._id;

    if (!messageId) {
        res.status(500).json({
            message: 'Fill please messageId field'
        });
        return;
    }

    let candidate = await Message.findOne({'_id': messageId});

    if (!candidate) {
        res.status(404).json({
            message: 'Bad messageId'
        });
        return;
    }

    if ( currentUserId !== candidate.sender.toString() ) {
        res.status(500).json({
            message: 'It not yours message'
        });
        return;
    }

    candidate.content = content;

    try {
        await candidate.save();
    } catch (e) {
        console.error(e);
        await res.status(500).json({
            'message': 'Error in message saving'
        });
        return;
    }


    res.status(200).json(candidate);
};

module.exports.deleteMessage = async (req, res) => {
    const messageId = req.params.id;
    const currentUserId = req.user._id;

    if (!messageId) {
        res.status(500).json({
            message: 'Fill please messageId field'
        });
        return;
    }

    let candidate = await Message.findOne({'_id': messageId});

    if (!candidate) {
        res.status(404).json({
            message: 'Bad messageId'
        });
        return;
    }

    if ( currentUserId !== candidate.sender.toString() ) {
        res.status(500).json({
            message: 'It not yours message'
        });
        return;
    }

    candidate.status = 'deleted';

    try {
        await candidate.save();
    } catch (e) {
        console.error(e);
        await res.status(500).json({
            'message': 'Error in message saving'
        });
        return;
    }


    res.status(200).json(candidate);
};

// todo: add pagination
module.exports.getAll = async (req, res) => {
    const senderId = req.user._id;
    let sender = await User.findOne({ _id: senderId }).select('+chats');

    const chats = await Chat.find({
        '_id': { $in: sender.chats },
    }, {
        'messages': { $slice: -15 }
    }).populate('messages');

    chats.every(chat => chat.messages = chat.messages.filter( message => message.status !== 'deleted'))

    let response = { ...chats };

    await res.status(200).json(response);
};

// get chat info by recipient id
module.exports.getChat = async (req, res) => {
    const chatId = req.params.chatId;
    const offset = typeof req.params.offset !== 'undefined' ? req.params.offset : 0;

    const chat = await Chat.findOne({
            '_id': chatId
        },
        {
            'messages': { $slice: [-offset - 15, 15] }
        }).populate("interlocutors messages");

    if (chat) {
        await res.status(200).json(chat);
    } else {
        await res.status(404).json({
            message: 'chat not found'
        });
    }
};

//get current user dialogs with other user, by current user id
module.exports.getCollabChats = async (req, res) => {
    const senderId = req.user._id;
    const interlocutor = req.params.id;
    const sender =
        await User
            .findOne({ _id: senderId })
            .select('+chats')
            .populate('chats');
    const candidate = sender.chats.find(chat => chat._id === interlocutor);
    res.json({...candidate});
}

module.exports.createChat = async (req, res) => {
    const recipientId = req.body.recipient;
    const senderId = req.user._id;

    const chat = await createChat(recipientId, senderId);

    if (chat) {
        let response = {
            chat,
            message : 'chat successfully created'
        };
        await res.status(200).json(response);
    } else {
        await res.status(400).json({
            'message': 'chat already created'
        });
    }
};

module.exports.deleteChat = async (req, res) => {
    const chatId = req.params.chatId;
    const chat = await Chat.findOne({_id: chatId});

    try {
        await Chat.deleteOne({_id: chatId});

        await Message.deleteMany({_id: chat.messages})


        chat.interlocutors.forEach(
            user => Sockets.sendToUser(user._id, 'deleteChat', chatId)
        );
    } catch (e) {
        res.status(500).json({
            message: 'Error on deleting'
        });
    }

    if (!chat) {
        res.status(404).json({
            message: 'Chat not found'
        });
    }

    res.status(200).json({
        message: 'success'
    });
};

module.exports.readMessage = async (req, res) => {
    const messageId = req.params.id;
    const message = await Message.findOne({_id: messageId});
    message.status = 'read';
    await message.save();

    const chat = await Chat.findOne({ "messages" : messageId })

    const recipient = chat.interlocutors.find(userId => String(userId) !== String(req.user._id))

    Sockets.sendToUser( recipient, SocketTypes.changeMessageStatus, {
        message: message,
        destination: chat._id,
        status: 'read'
    });

    res.status(200).json({
        message : 'success'
    })
}
