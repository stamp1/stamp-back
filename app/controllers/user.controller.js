const clients       = require('../middlewares/auth.middleware').clients;
const User          = require('../models/user.model');
const chat        = require('../models/chat.model');
const { getFormData, saveFile }   = require('../services/upload')
const bcrypt        = require('bcrypt');
const fs            = require('fs');

// get current user data  (req.user - data about current session)
module.exports.getCurrentUserData = async (req, res) => {
    let user = await User
        .findOne({_id: req.user._id})
        .select('+options')
    if (user) {
        res.status(200).json(user);
    } else {
        res.status(404).json({message: 'User not found.'});
    }
    return;
};

// TODO: make route - get user data by id
module.exports.getUserDataById = async (req, res) => {
    res.status(500).send('This controller dont ready');
}

// TODO: make route - update data
module.exports.updateUserData = async (req, res) => {
    const user = await User.findOne({ _id: req.user._id});

    if (req.body.login) {
        const newLogin = req.body.login;
        const oldLogin = user.login;
        const candidate = await User.findOne({login: newLogin});
        if (!candidate) {
            user.login = newLogin;
        } else {
            res.status(409).json({
                user,
                message: 'Login is busy.'
            });
            return;
        }
    }

    if (req.body.password) {
        const salt = bcrypt.genSaltSync(10);
        const userPassword = bcrypt.hashSync(req.body.password, salt);
        console.log(req.body.password);
        console.log(userPassword);
        user.password = userPassword;
    }

    const response = await user.save();
    if (response) {
        res.status(200).json({
            user: response,
            message: 'User changed successfully.'
        });
    }
    return;
}

module.exports.updateUserProfileImage = async (req, res) => {
    const formData = await getFormData(req, {
        multiples: false,
        maxFileSize: 20 * 1024 * 1024
    });

    let err = 0;

    if (!formData.files.avatar) err = 1
    if (!~formData.files.avatar.type.indexOf('image')) err = 2

    if (err) {
        for (let key in formData.files) {
            const path = formData.files[key].path;
            fs.unlinkSync(path);
        }
        return;
    }

    const file = await saveFile(formData.files.avatar, req.user);

    const user = await User.findOne({ _id: req.user._id });

    user.options.image = file._id;

    try {
        await user.save();
    } catch (e) {
        console.log(e);
    }

    res.status(200).json({message: 'fuck off', file});
    return;
}
// deleteUser

// TODO: make route - detele user
module.exports.deleteUser = async (req, res) => {
    res.status(500).send('This controller dont ready');
}

// rearch by user login
// req need to have query field in body
module.exports.searchUserByLogin = async (req, res) => {
    let query = req.params.query;

    let candidates = await User.find({
        login: {
            "$regex": query,
            "$options": "i",
            "$ne": req.user.login,
        }
    });

    let response = {
        candidates
    };
    res.status(200).json(response);
};


// test route (send emit to each user, ws)
module.exports.notifyAll = (req, res) => {
    res.status(500).send('This controller dont ready');
};


module.exports.getFriends = async (req, res) => {
    let user = await User
        .findOne({_id: req.user._id})
        .select('+chats')
        .populate('chats')

    const friends = [];
    user.chats.forEach(chat => chat.interlocutors.forEach(interlocutor => {
        if (String(interlocutor) !== String(user._id)) friends.push(interlocutor);
    }))
    const response = await User.find({_id: {$in : friends}});
    res.status(200).json(response)
}
