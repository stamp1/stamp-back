const File      = require('../models/file.model')

module.exports.getFile = async (req, res) => {
    const fileId = req.params.fileId;
    const quality = req.params.quality;

    const fileInstance = await File.findOne({ _id: fileId });

    res.sendFile(fileInstance.path);

    // res.status(400).json({message: 'errrror'});
};