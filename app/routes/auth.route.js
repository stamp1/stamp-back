const express       = require('express');
const router        = express.Router();
const controller    = require('../controllers/auth.controller');

router.post('/login', controller.login);
router.post('/register', controller.register);
router.post('/logout', controller.logout);
router.get('/google-auth', controller.getGoogleAuth);
router.get('/google-redirect', controller.googleRedirectCallback);

module.exports = router;
