const express       = require('express');
const router        = express.Router();
const controller    = require('../controllers/payment.controller');

router.post('/indent', controller.createIndent);

module.exports = router;
