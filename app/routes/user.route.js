const express       = require('express');
const router        = express.Router();
const controller    = require('../controllers/user.controller');

// this routes reserved
// they have authentication check

router.get('/', controller.getCurrentUserData);
router.get('/friends', controller.getFriends);
router.post('/avatar', controller.updateUserProfileImage);
router.patch('/', controller.updateUserData);
router.delete('/', controller.deleteUser);
router.post('/notifyAll', controller.notifyAll);
router.get('/search/:query', controller.searchUserByLogin);
router.get('/:userId', controller.getUserDataById);

module.exports = router;
