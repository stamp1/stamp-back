const express       = require('express');
const router        = express.Router();
const controller    = require('../controllers/file.controller');

router.get(['/:fileId/:quality', '/:fileId'], controller.getFile);

module.exports = router;
