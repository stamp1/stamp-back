const express       = require('express');
const router        = express.Router();
const controller    = require('../controllers/chat.controller.js');

router.get('/getCollab/:id', controller.getCollabChats);
router.get('/', controller.getAll);
router.put('/', controller.createChat);
router.put('/message/', controller.sendMessage);
router.patch('/message/read/:id', controller.readMessage);
router.patch('/message/:messageId', controller.changeMessage);
router.delete('/message/:id', controller.deleteMessage);
router.delete('/:chatId', controller.deleteChat);
router.get('/:chatId/:offset', controller.getChat);

module.exports = router;
