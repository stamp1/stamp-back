const path = require("path");
module.exports = {
    server: {
        host: '0.0.0.0',
        port: process.env.HTTPS_PORT || 443,
    },
    db: {
        mongodbURI: process.env.USER === 'ddri' ? 'mongodb://127.0.0.1:27017/' : 'mongodb://mongo:27017/',
        options: {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        }
    },
    passport: {
        privateKey: process.env.PASSPORT_PRIVATE,
        options: {
            expiresIn: 60 * 60
        }
    },
    ws: {
        port: 4848,
        path: '/ws'
    },
    redis: {
        host: process.env.USER === 'ddri' ? '0.0.0.0' : 'redis',
    },
    certs: {
        cert: process.env.IS_TEST ? path.join(__dirname+'/cert.pem'): path.join(__dirname+'/fullchain.pem'),
        key: process.env.IS_TEST ? path.join(__dirname+'/key.pem') : path.join(__dirname+'/privkey.pem')
    }
};
